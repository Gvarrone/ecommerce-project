<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ProductsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/remove', [CartController::class, 'removeCart'])->name('removeProduct');

Route::get('/',[ProductsController::class, 'index'])->name('main');
Route::get('/products/detail/{product}', [ProductsController::class, 'productDetail'])->name('detail');
Route::get('/checkout', [ProductsController::class,'checkout'])->name('checkout');

//Cart Controller
Route::get('/cart', [CartController::class,'cartList'])->name('cart.list');
Route::post('/cart', [CartController::class,'addToCart'])->name('addtocart');
Route::post('/update-cart', [CartController::class, 'updateCart'])->name('cart.update');
Route::post('/clear', [CartController::class, 'clearAllCart'])->name('cart.clear');





//Full text search
Route::get('/searchresult', [ProductsController::class, 'search'])->name('search');


//Laravel ui routes
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
