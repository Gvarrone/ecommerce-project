<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'name',
        'price',
        'brand',
        'description',
        'stock',
        'img',
    ];

    public function toSearchableArray(){
        $products = $this->pluck('name')->join(', ');

        $array = [
            'id' => $this->id,
            'name'=> $this->name,
            'brand' => $this->brand, 
        ];

        return $array;
    }

}
