<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use App\Http\Requests\CartRequest;

class CartController extends Controller
{
   
    public function cartList(){
        $cartItems = \Cart::getContent();
        return view('cart', compact('cartItems'));
    }

    public function addToCart(CartRequest $request){
        \Cart::add([
            'id'=> $request->id,
            'name'=>$request->name,
            'price'=>$request->price,
            'brand'=>$request->brand,
            'description'=>$request->description,
            'quantity'=>$request->quantity,
            'img'=>$request->img
        ]);

        session()->flash('success', 'Il prodotto è stato aggiunto al carrello');

        return redirect()->route('cart.list');
    }
    
    public function removeCart(CartRequest $request){
        dd($request);
        Cart::remove($request->id);
        session()->flash('success', 'Il prodotto è stato rimosso dal carrello');
        return redirect()->route('cart.list');
    }

    public function updateCart(CartRequest $request){
        \Cart::update(
            $request->id,
            ['quantity'=>['relative'=> false, 'value'=>$request->quantity]]
        );

        session()->flash('success', 'Il carrello è stato aggiornato');

        return redirect()->route('cart.list');
    }


    public function clearAllCart(){
        \Cart::clear();

        session()->flash('success', 'Il carrello è stato svuotato!');

        return redirect()->route('cart.list');

    }

}
