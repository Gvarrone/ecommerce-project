<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(){
        $products= Product::all();
        return view('main',compact('products'));
    }

    public function productDetail(Product $product){
        return view('detail', compact('product'));
    }

    public function cart(){
        return view('cart');
    }
    public function checkout(){
        return view('checkout');
    }

    public function search(Request $request){
        $itemId = [];
        $query = $request->input('search');
        $products = Product::search($query)->get();
        foreach($products as $product){
            array_push($itemId, $product->id);
        };
        $randomProducts = Product::inRandomOrder()->whereNotIn('id', $itemId)->take(5)->get();
        return view('results', compact('query', 'products', 'randomProducts'));
    }
}
