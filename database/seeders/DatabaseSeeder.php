<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name'=>'Hp pavillion Aero', 
                'price'=>'1099',
                'brand'=>'Hp',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'10',
                'img'=>'https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcSD9-NLUyrseU7oXx0VPYf3hhVUq1E8yaufxP-lwRM_UpjsjZqR5iB5yrtvUQewSNGlqWEvOEjZ-Q&usqp=CAc'
            ],
            [
                'name'=>'Chuwi laptop Gemibook', 
                'price'=>'569',
                'brand'=>'Chewi',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'6',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Dell Latitude 3420', 
                'price'=>'789',
                'brand'=>'Dell',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'2',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Microsft Surface', 
                'price'=>'799',
                'brand'=>'Microsoft',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'1',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Lenovo Ideapad 15"', 
                'price'=>'929',
                'brand'=>'Lenovo',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'4',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'HP 470 G7', 
                'price'=>'329',
                'brand'=>'HP',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'9',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Dell Xps 139300', 
                'price'=>'479',
                'brand'=>'Dell',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'5',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Lenovo V15', 
                'price'=>'1299',
                'brand'=>'Lenovo',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'3',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Dell Inspiron 14', 
                'price'=>'849',
                'brand'=>'Dell',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'1',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Apple Imac', 
                'price'=>'2099',
                'brand'=>'Apple',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'2',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Apple Iphone 13 proMax', 
                'price'=>'1399',
                'brand'=>'Apple',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'3',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Acer 3412X', 
                'price'=>'299',
                'brand'=>'Acer',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'25',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
            ],
            [
                'name'=>'Microsoft Surface Go', 
                'price'=>'589',
                'brand'=>'Chewi',
                'description'=>'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cum ullam saepe doloribus ex laboriosam repudiandae nam temporibus soluta. Dolor ut quasi modi. Ipsum earum ut commodi doloribus. Labore, eveniet tempore!',
                'stock'=>'1',
                'img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZXYj_pgsx5Xlwhqzszn9EKRrptoGt9bRtuA&usqp=CAU'
                ]  
    
            ];
    
            foreach($products as $product){
                DB::table('products')->insert(
                    [
                    'name'=>$product['name'],
                    'price'=>$product['price'],
                    'brand'=>$product['brand'],
                    'description'=>$product['description'],
                    'stock'=>$product['stock'],
                    'img'=>$product['img'],
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now()
                    ]
                );
            };
    }
}
