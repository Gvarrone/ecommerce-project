<x-app>
    <x-slot name="title">{{$product->name}}</x-slot>
    <div class="container mt-5">
        <div class="row">
            <h1>{{$product->name}}</h1>
            <p>{{$product->brand}}</p>
            <div class="col-8">
                <img src="{{$product->img}}" alt="{{$product->name}} foto">
            </div>
            <div class="col-4 d-flex align-items-center">
                <div class="card card-border p-5">
                    <h3>Prezzo : € {{$product->price}},00</h4>
                    <div class="badge bg-info">{{$product->stock}} Disponibili</div>
                    <form action="{{route('addtocart')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="text" name="id" value="{{$product->id}}" hidden>
                        <input type="text" name="name" value="{{$product->name}}" hidden>
                        <input type="text" name="description" value="{{$product->description}}" hidden>
                        <input type="text" name="price" value="{{$product->price}}" hidden>
                        <input type="text" name="brand" value="{{$product->brand}}" hidden>
                        <input type="text" name="img" value="{{$product->img}}" hidden>
                        <div class="d-flex align-items-center justify-content-center">
                            <label for="quantity">Inserisci la quantità</label>
                            <input type="number" name="quantity" value="1" max="{{$product->stock}}">
                        </div>
                        <div class="d-flex align-items-center justify-content-center mt-3">
                            <button class="btn btn-success">Aggiungi al carrello</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                {{$product->description}}
            </div>
        </div>
    </div>
</x-app>