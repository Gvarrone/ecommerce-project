<x-app>
    <x-slot name="title">Check out</x-slot>
    <div class="container">
        <div class="row">
            <div class="col-8 card card-border p-3">
                <form class="row g-3">
                    <div class="col-md-6">
                      <label for="inputEmail4" class="form-label">Nome e cognome</label>
                      <input type="email" class="form-control" id="inputname">
                    </div>
                    <div class="col-md-6">
                      <label for="inputPassword4" class="form-label">Email</label>
                      <input type="password" class="form-control" id="inputmail">
                    </div>
                    <div class="col-12">
                      <label for="inputAddress" class="form-label">Address</label>
                      <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>
                    <div class="col-12">
                      <label for="inputAddress2" class="form-label">Address 2</label>
                      <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                    </div>
                    <div class="col-md-6">
                      <label for="inputCity" class="form-label">City</label>
                      <input type="text" class="form-control" id="inputCity">
                    </div>
                    <div class="col-md-4">
                      <label for="inputState" class="form-label">State</label>
                      <select id="inputState" class="form-select">
                        <option selected>Choose...</option>
                        <option>Italy</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label for="inputZip" class="form-label">Zip</label>
                      <input type="text" class="form-control" id="inputZip">
                    </div>
                    <div class="col-12 d-flex justify-content-end">
                      <button type="submit" class="btn btn-primary">Completa acquisto</button>
                    </div>
                  </form>
            </div>
            <div class="col-3 offset-1 card card-border p-3">
                <div class="row">
                    <div class="col-6">
                        <h6>Prodotti</h6>
                    </div>
                    <div class="col-3">
                        <h6>N°</h6>
                    </div>
                    <div class="col-3">
                        <h6>Prezzo</h6>
                    </div>
                </div>
                <div class="row border-bottom border-dark">
                    <div class="col-6">
                        <p>Microsoft surface</p>
                    </div>
                    <div class="col-3">
                        <p>1</p>
                    </div>
                    <div class="col-3">
                        <p>€ 1099</p>
                    </div>
                </div>
                <div class="row border-bottom border-dark">
                    <div class="col-6">
                        <p>Microsoft surface</p>
                    </div>
                    <div class="col-3">
                        <p>1</p>
                    </div>
                    <div class="col-3">
                        <p>€ 1099</p>
                    </div>
                </div>
                <div class="row border-bottom border-dark">
                    <div class="col-6">
                        <p>Microsoft surface</p>
                    </div>
                    <div class="col-3">
                        <p>1</p>
                    </div>
                    <div class="col-3">
                        <p>€ 1099</p>
                    </div>
                </div>
                <div class="row border-bottom border-dark">
                    <div class="col-6">
                        <p>Microsoft surface</p>
                    </div>
                    <div class="col-3">
                        <p>1</p>
                    </div>
                    <div class="col-3">
                        <p>€ 1099</p>
                    </div>
                </div>
                <div class="row border-bottom border-dark">
                    <div class="col-6">
                        <p>Microsoft surface</p>
                    </div>
                    <div class="col-3">
                        <p>1</p>
                    </div>
                    <div class="col-3">
                        <p>€ 1099</p>
                    </div>
                </div>
                <div class="row h-100 align-items-end">
                    <div class="col-8">
                        <h5>Totale</h5>
                    </div>
                    <div class="col-4">
                        <h5>
                            € 2000
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app>