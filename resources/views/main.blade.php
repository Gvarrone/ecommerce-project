<x-app>
    <x-slot name="title">GiammyShop Home</x-slot>
    <section id="masthead">
        <div class="container masthead vh-75 card-border mt-3">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <h1 class="text-white text-center">Acquista Pc e Tanto altro</h1>
                    <div class="col-12 d-flex justify-content-center">
                        <form method="GET" action="{{route('search')}}">
                            @csrf
                            <input type="text" class="search" name="search">
                            <button type="submit" class="btn btn-warning">Cerca</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="productWrapper">
        <div class="container mt-5">
            <div class="row">
                <h2>
                    Prodotti suggeriti
                </h2>
                @foreach ($products as $product)
                    <div class="col-3 mt-3">
                        <div class="card card-border" style="width: 18rem;">
                            <img src="{{$product->img}}" class="card-img-top card-img" alt="{{$product->name}} foto">
                            <div class="card-body">
                                <p class="card-text fw-bold">{{$product->brand}}</p>
                                <h5 class="card-title">{{$product->name}}</h5>
                                <p class="card-text">{{$product->description}}</p>
                                <div class="badge bg-info">{{$product->stock}} Disponibili</div>
                                <div class="d-flex justify-content-between">
                                    <h5>€ {{$product->price}},00</h5>
                                    <a href="{{route('detail', compact('product'))}}" class="btn btn-primary">Vedi prodotto</a>
                                </div>
                            </div>
                        </div>    
                    </div>
                @endforeach
            </div>
        </div>
    </section> 
</x-app>