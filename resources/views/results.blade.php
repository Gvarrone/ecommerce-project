<x-app>
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <h1>Risultati di ricerca per : {{$query}}</h1>
            </div>
        </div>
        <div class="row">
            @foreach ($products as $product)
            <div class="col-3 mt-3">
                <div class="card card-border" style="width: 18rem;">
                    <img src="{{$product->img}}" class="card-img-top card-img" alt="{{$product->name}} foto">
                    <div class="card-body">
                        <p class="card-text fw-bold">{{$product->brand}}</p>
                        <h5 class="card-title">{{$product->name}}</h5>
                        <p class="card-text">{{$product->description}}</p>
                        <div class="badge bg-info">{{$product->stock}} Disponibili</div>
                        <div class="d-flex justify-content-between">
                            <h5>€ {{$product->price}},00</h5>
                            <a href="{{route('detail', compact('product'))}}" class="btn btn-primary">Vedi prodotto</a>
                        </div>
                    </div>
                </div>    
            </div>
            @endforeach
        </div>
        <div class="row">
            <h1>Gli utenti che hanno ricercato {{$query}}, hanno visto anche : </h1>
            @foreach ($randomProducts as $product)
            <div class="col-3 mt-3">
                <div class="card card-border" style="width: 18rem;">
                    <img src="{{$product->img}}" class="card-img-top card-img" alt="{{$product->name}} foto">
                    <div class="card-body">
                        <p class="card-text fw-bold">{{$product->brand}}</p>
                        <h5 class="card-title">{{$product->name}}</h5>
                        <p class="card-text">{{$product->description}}</p>
                        <div class="badge bg-info">{{$product->stock}} Disponibili</div>
                        <div class="d-flex justify-content-between">
                            <h5>€ {{$product->price}},00</h5>
                            <a href="{{route('detail', compact('product'))}}" class="btn btn-primary">Vedi prodotto</a>
                        </div>
                    </div>
                </div>    
            </div>
            @endforeach
        </div>
    </div>
</x-app>