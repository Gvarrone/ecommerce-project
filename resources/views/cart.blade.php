<x-app>
    {{-- @dd(Auth::user()); --}}
    <x-slot name="title">Carrello</x-slot>
    <div class="container">
        <h1 class="p-3">Carrello</h1>
        <div class="row">
            <div class="col-12 card card-border">
                <div class="row p-3">
                    <div class="col-3">
                        <h5 class="text-center">Prodotto</h5>
                    </div>
                    <div class="col-3">
                        <h5 class="text-center">Quantità</h5>
                    </div>
                    <div class="col-3">
                        <h5 class="text-center">Prezzo</h5>
                    </div>
                    <div class="col-3">
                        <h5 class="text-center">Modifica</h5>
                    </div>
                </div>
                @foreach ($cartItems as $product)    
                    <div class="row mt-3 border-bottom border-dark p-3">
                        <div class="col-3">
                            <p class="text-center">{{$product->name}}</p>
                        </div>
                        <div class="col-3">
                            <p class="text-center">{{$product->quantity}}</p>
                        </div>
                        <div class="col-3">
                            <p class="text-center">{{$product->price * $product->quantity}},00 €</p>
                        </div>
                        
                        <div class="col-3 d-flex justify-content-center">
                            <form action="{{route('removeProduct')}}" method="POST">
                                @csrf
                                <input type="number" value="{{$product->id}}" name="id" hidden>
                                <button class="btn btn-danger" type="submit">Elimina</button>
                            </form>
                        </div>
                    </div>
                @endforeach
                <div class="row mt-3 p-3">
                    <div class="col-8 d-flex justify-content-between">
                        <h5 class="text-start">Totale Carrello</h5>
                        <h5 class="text-end">{{Cart::getSubTotal()}},00 €</h5>
                    </div>
                    <div class="col-4 d-flex justify-content-evenly">
                        <a href="{{route('main')}}" class="btn btn-primary">Torna indietro</a>
                        <a href="{{route('checkout')}}" class="btn btn-success">Checkout</a>
                        <form action="{{route('cart.clear')}}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-warning">Svuota carrello</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app>